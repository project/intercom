
Intercom for Drupal 8
---------------------
Integrate Intercom with your Drupal website. The Intercom module provides an
easy method to set-up the live chat functionality for the Intercom service and
provides the Intercom PHP-SDK as a Drupal service for integrating modules.

https://www.drupal.org/project/intercom
